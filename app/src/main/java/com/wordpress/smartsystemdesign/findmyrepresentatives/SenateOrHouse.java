package com.wordpress.smartsystemdesign.findmyrepresentatives;

/**
 * Created by Mammoth on 5/19/2015.
 */
public enum SenateOrHouse
{
    Senate,
    House
}