package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Map;

//TODO: change AdapterView to AdapterViewCompat to support older versions of Android
public class StateSearchActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener
{
    Spinner stateSpinner;
    Spinner houseSenateSpinner;
    private boolean userIsInteracting;
    private boolean isSenator = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_search);

        stateSpinner = (Spinner)findViewById(R.id.state_spinner);
        stateSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, States.statesMap.values().toArray(new String[0]));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(spinnerArrayAdapter);

        houseSenateSpinner = (Spinner)findViewById(R.id.house_senate_spinner);
        houseSenateSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> spinnerArrayAdapter2 = ArrayAdapter.createFromResource(this,
                R.array.House_Senate_array, android.R.layout.simple_spinner_item);
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        houseSenateSpinner.setAdapter(spinnerArrayAdapter2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_state_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserInteraction()
    {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        Intent intent;

        if(userIsInteracting)  // check if user is selecting from spinner or if this callback is firing on first layout
        {
            if(parent == stateSpinner)
            {
                intent = new Intent(this, ResultsActivity.class);
                String twoDigitState = getTwoDigitState(parent.getItemAtPosition(position).toString());
                if (twoDigitState != null)
                {
                    intent.putExtra("state", twoDigitState);
                    intent.putExtra("isSenator", isSenator);
                    startActivity(intent);
                } else
                {
                    Log.d("StateSearchActivity", "Couldn't find state.");
                }
            }
            else if(parent == houseSenateSpinner)
            {
                if(position == 0)
                {
                    isSenator = true;
                }
                else
                {
                    isSenator = false;
                }
            }
        }
    }

    private String getTwoDigitState(String state)
    {
        for (Map.Entry<String, String> stateEntry : States.statesMap.entrySet())
        {
            if(stateEntry.getValue().equals(state))
            {
                return stateEntry.getKey();
            }
        }
        return null;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
    }
}