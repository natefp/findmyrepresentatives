package com.wordpress.smartsystemdesign.findmyrepresentatives;

/**
 * Created by Mammoth on 5/18/2015.
 */
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

// here's our beautiful adapter
public class ArrayAdapterItem extends ArrayAdapter<Representative>
{
    Context context;
    int layoutResourceId;
    ArrayList<Representative> reps = new ArrayList<Representative>();

    public ArrayAdapterItem(Context context, int layoutResourceId, ArrayList reps)
    {
        super(context, layoutResourceId, reps);

        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.reps = reps;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolderItem viewHolder;

        if(convertView == null)
        {
            // inflate the layout
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);

            // well set up the ViewHolder
            viewHolder = new ViewHolderItem();
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.textViewState = (TextView) convertView.findViewById(R.id.state);
            viewHolder.textViewDistrict = (TextView) convertView.findViewById(R.id.district);

            // store the holder with the view.
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        // object item based on the position
        Representative representative = reps.get(position);

        if(representative != null)
        {
            viewHolder.textViewState.setText(representative.state);
            viewHolder.textViewDistrict.setText(", " + representative.districtOrSeat);
            viewHolder.textViewName.setText(representative.nameFormatted);

            if (representative.party == Party.Republican)
            {
                viewHolder.textViewName.setTextColor(Color.RED);
            }
            else if (representative.party == Party.Democrat)
            {
                viewHolder.textViewName.setTextColor(Color.BLUE);
            }
            else
            {
                viewHolder.textViewName.setTextColor(Color.BLACK);
            }

            viewHolder.textViewName.setTag(representative.state);
        }

        return convertView;
    }
}

class ViewHolderItem {
    TextView textViewName;
    TextView textViewState;
    TextView textViewDistrict;
}