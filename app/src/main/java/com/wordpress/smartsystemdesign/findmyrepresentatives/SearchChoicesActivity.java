package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SearchChoicesActivity extends ActionBarActivity
{

    private ImageButton buttonAutoFind;
    private Button buttonFindByZip;
    private Button buttonFindByName;
    private Button buttonFindByState;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_choices);
        buttonAutoFind = (ImageButton)findViewById(R.id.auto_find);
        buttonFindByZip = (Button)findViewById(R.id.find_by_zip);
        buttonFindByName = (Button)findViewById(R.id.find_by_name);
        buttonFindByState = (Button)findViewById(R.id.find_by_state);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_choices, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickFindButton(View view)
    {
        Intent intent;

        if(view == buttonAutoFind)
        {
            GPSTracker gpsTracker = new GPSTracker(this);
            String zip = null;

            if (gpsTracker.getIsGPSTrackingEnabled())
            {
                zip = gpsTracker.getPostalCode(this);
            }

            intent = new Intent(this, ResultsActivity.class);

            intent.putExtra("zip", zip);
        }
        else if(view == buttonFindByZip)
        {
            intent = new Intent(this, ZipSearchActivity.class);
        }
        else if(view == buttonFindByName)
        {
            intent = new Intent(this, NameSearchActivity.class);
        }
        else if(view == buttonFindByState)
        {
            intent = new Intent(this, StateSearchActivity.class);
        }
        else
        {
            return;
        }

        startActivity(intent);
    }
}