package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


public class ZipSearchActivity extends ActionBarActivity
{
    EditText zipEditText;
    Button findButton;
    final int zipLength = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zip_search);
        zipEditText = (EditText)findViewById(R.id.zip_edit_text);
        findButton = (Button)findViewById(R.id.find_button);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_zip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickFindButton(View view)
    {
        Intent intent;

        if(view == findButton)
        {
            intent = new Intent(this, ResultsActivity.class);

            String zipText = zipEditText.getText().toString();
            if (zipText.length() == zipLength)
            {
                intent.putExtra("zip", zipText);
                startActivity(intent);
            }
            else
            {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                // set title
                alertDialogBuilder.setTitle(R.string.fix_zip_dialog_title);

                // set dialog message
                alertDialogBuilder
                        .setMessage(R.string.fix_zip_dialog_message)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener()
                        {
                            public void onClick(DialogInterface dialog, int id)
                            {
                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        }
        else
        {
            return;
        }
    }
}