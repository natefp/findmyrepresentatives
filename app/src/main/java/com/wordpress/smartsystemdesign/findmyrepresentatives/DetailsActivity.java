package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;


public class DetailsActivity extends ActionBarActivity
{
    Representative representative;
    TextView name;
    TextView state;
    TextView senateOrHouse;
    TextView ditrictOrSeat;
    ImageButton phone;
    ImageButton textMessage;
    ImageButton address;
    ImageButton website;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        name = (TextView)findViewById(R.id.detail_name);
        state = (TextView)findViewById(R.id.detail_state);
        senateOrHouse = (TextView)findViewById(R.id.detail_senate_house);
        ditrictOrSeat = (TextView)findViewById(R.id.detail_district_or_seat);

        Intent intent = getIntent();

        if(intent.hasExtra("rep"))
        {
            representative = (Representative) intent.getSerializableExtra("rep");

            name.setText(representative.nameFormatted);

            if (representative.party == Party.Republican)
            {
                name.setTextColor(Color.RED);
            }
            else if (representative.party == Party.Democrat)
            {
                name.setTextColor(Color.BLUE);
            } else
            {
                name.setTextColor(Color.BLACK);
            }

            state.setText(representative.state);

            if(representative.senateOrHouse == SenateOrHouse.Senate)
            {
                senateOrHouse.setText("Senator");
            }
            else
            {
                senateOrHouse.setText("House of Representatives");
            }
            ditrictOrSeat.setText(representative.districtOrSeat);
        }
    }

    public void onClickPhoneButton(View view)
    {
        String uri = "tel:" + representative.phone;
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(uri));

        startActivity(callIntent);
    }

    public void onClickTextMessageButton(View view)
    {
        Uri uri = Uri.parse("smsto:" + representative.phone);
        Intent textMessageIntent = new Intent(Intent.ACTION_SENDTO, uri);
//        textMessageIntent.putExtra("sms_body", "Dear " + (representative.senateOrHouse == SenateOrHouse.Senate ? "Senator, " : "Representative, "));
        startActivity(textMessageIntent);
    }

    public void onClickAddressButton(View view)
    {
        Uri location = Uri.parse(buildUriStreetAddressString());
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
        startActivity(mapIntent);
    }

    private String buildUriStreetAddressString()
    {
        return "geo:0,0?q=" + representative.streetAddress.replace(" ", "+");
    }

    public void onClickWebsiteButton(View view)
    {
        Intent websiteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(representative.website));
        startActivity(websiteIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}