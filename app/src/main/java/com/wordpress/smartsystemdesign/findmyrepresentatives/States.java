package com.wordpress.smartsystemdesign.findmyrepresentatives;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Mammoth on 5/16/2015.
 */
public class States
{
    static final Map<String, String> statesMap;
    static
    {
        statesMap = new LinkedHashMap<String, String>();
        statesMap.put("AL", "Alabama");
        statesMap.put("AK", "Alaska");
        statesMap.put("AZ", "Arizona");
        statesMap.put("AR", "Arkansas");
        statesMap.put("CA", "California");
        statesMap.put("CO", "Colorado");
        statesMap.put("CT", "Connecticut");
        statesMap.put("DE", "Delaware");
        statesMap.put("FL", "Florida");
        statesMap.put("GA", "Georgia");
        statesMap.put("HI", "Hawaii");
        statesMap.put("ID", "Idaho");
        statesMap.put("IL", "Illinois");
        statesMap.put("IN", "Indiana");
        statesMap.put("IA", "Iowa");
        statesMap.put("KS", "Kansas");
        statesMap.put("KY", "Kentucky");
        statesMap.put("LA", "Louisiana");
        statesMap.put("ME", "Maine");
        statesMap.put("MD", "Maryland");
        statesMap.put("MA", "Massachusetts");
        statesMap.put("MI", "Michigan");
        statesMap.put("MN", "Minnesota");
        statesMap.put("MS", "Mississippi");
        statesMap.put("MO", "Missouri");
        statesMap.put("MT", "Montana");
        statesMap.put("NE", "Nebraska");
        statesMap.put("NV", "Nevada");
        statesMap.put("NH", "New Hampshire");
        statesMap.put("NJ", "New Jersey");
        statesMap.put("NM", "New Mexico");
        statesMap.put("NY", "New York");
        statesMap.put("NC", "North Carolina");
        statesMap.put("ND", "North Dakota");
        statesMap.put("OH", "Ohio");
        statesMap.put("OK", "Oklahoma");
        statesMap.put("OR", "Oregon");
        statesMap.put("PA", "Pennsylvania");
        statesMap.put("RI", "Rhode Island");
        statesMap.put("SC", "South Carolina");
        statesMap.put("SD", "South Dakota");
        statesMap.put("TN", "Tennessee");
        statesMap.put("TX", "Texas");
        statesMap.put("UT", "Utah");
        statesMap.put("VT", "Vermont");
        statesMap.put("VA", "Virginia");
        statesMap.put("WA", "Washington");
        statesMap.put("WV", "West Virginia");
        statesMap.put("WI", "Wisconsin");
        statesMap.put("WY", "Wyoming");
    }
}