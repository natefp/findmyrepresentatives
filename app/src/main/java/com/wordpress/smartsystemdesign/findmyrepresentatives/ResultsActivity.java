package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResultsActivity extends ActionBarActivity implements RepDataReadyListener, AdapterView.OnItemClickListener
{
    ListView repDataListView;
    RetrieveRepData retrieveRepData;
    ArrayList<Representative> reps = new ArrayList<Representative>();
    private boolean isSenator = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        repDataListView = (ListView) findViewById(R.id.repList);

        Intent intent = getIntent();
        String zip;
        String name;
        String state;
        String url;

        if(intent.hasExtra("isSenator"))
        {
            isSenator = intent.getBooleanExtra("isSenator", true);
        }

        if(intent.hasExtra("zip"))
        {
            zip = intent.getStringExtra("zip");
            url = createUrlString(SearchOption.Zip, zip);
        }
        else if(intent.hasExtra("name"))
        {
            name = intent.getStringExtra("name");
            url = createUrlString(SearchOption.Name, name);
        }
        else if(intent.hasExtra("state"))
        {
            state = intent.getStringExtra("state");
            url = createUrlString(SearchOption.State, state);
        }
        else
        {
            Log.d("ResultsActivity", "No search results were found");
            url = "";  //TODO: fix this
        }

        retrieveRepData = new RetrieveRepData();
        retrieveRepData.setRepDataReadyListener(this);
        retrieveRepData.execute(url); //TODO: add code to ensure this is run on the UI thread (necessary for pre Jellybean)
        //TODO: add RetrieveRepData.cancel(true); if the back button is pressed to cancel http call
    }

    @Override
    public void repDataReady(List<Representative> reps)  // gets called after data is retrieved from website, xml parsed and put into ArrayList of Representatives
    {
        this.reps = (ArrayList) reps;

        ArrayAdapterItem adapter = new ArrayAdapterItem(this, R.layout.row_layout, (ArrayList)reps);
        ListView listViewItems = repDataListView;
        listViewItems.setAdapter(adapter);
        listViewItems.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("rep", (Serializable) reps.get(position));

        startActivity(intent);
    }

    private String createUrlString(SearchOption searchOption, String searchResultText)
    {
        String url = getResources().getString(R.string.domain);
        String getAllByZip = getResources().getString(R.string.get_all_by_zip);
        String getRepsByName = getResources().getString(R.string.get_reps_by_name);
        String getRepsByState = getResources().getString(R.string.get_reps_by_state);
        String getSensByName = getResources().getString(R.string.get_sens_by_name);
        String getSensByState = getResources().getString(R.string.get_sens_by_state);

        if(searchOption == SearchOption.Zip)
        {
            url += getAllByZip;
            url += "?zip=";
            url += searchResultText;
        }
        else if(searchOption == SearchOption.Name)
        {
            if(isSenator)
            {
                url += getSensByName;
            }
            else
            {
                url += getRepsByName;
            }
            url += "?name=";
            url += searchResultText;
        }
        else if(searchOption == SearchOption.State)
        {
            if(isSenator)
            {
                url += getSensByState;
            }
            else
            {
                url += getRepsByState;
            }
            url += "?state=";
            url += searchResultText;
        }
        return url;
    }

    private enum SearchOption
    {
        Zip, Name, State
    }
}