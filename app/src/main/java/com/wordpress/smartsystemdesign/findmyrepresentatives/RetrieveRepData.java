package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Mammoth on 5/15/2015.
 */
class RetrieveRepData extends AsyncTask<String, Void, InputStream>
{
    HttpURLConnection urlConnection;
    private RepDataReadyListener repDataReadyListener;

    @Override
    protected InputStream doInBackground(String... urls)
    {
        try
        {
            URL url = new URL(urls[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            return inputStream;
        }
        catch(MalformedURLException e)
        {
            Log.d("ResultsActivity", "Error: URL was malformed.");
            return null;
        }
        catch(IOException e)
        {
            Log.d("ResultsActivity", "Error: Could not open Http connection.");
            return null;
        }
        catch (Exception e)
        {
            Log.d("ResultsActivity", "Error: " + "e.toString()");
            e.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(InputStream repData)  // gets called after data is read from the internet
    {
        if(urlConnection != null)
        {
            urlConnection.disconnect();
        }
        try
        {
            repDataReadyListener.repDataReady(setupParserAndParseData(repData));
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setRepDataReadyListener(RepDataReadyListener listener)
    {
        repDataReadyListener = listener;
    }

    private List<Representative> setupParserAndParseData(InputStream repData) throws XmlPullParserException, IOException
    {
        try
        {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(repData, null);
            parser.nextTag();
            return parseXml(parser);
        }
        finally
        {
            repData.close();
        }
    }

    private List<Representative> parseXml(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        List<Representative> reps = new ArrayList<Representative>();

        while(parser.next() != XmlPullParser.END_DOCUMENT)
        {
            if(parser.getEventType() == XmlPullParser.START_TAG)
            {
                String name = parser.getAttributeValue(null, "name");
                String party = parser.getAttributeValue(null, "party");
                String twoDigitState = parser.getAttributeValue(null, "state");
                String district = parser.getAttributeValue(null, "district");
                String phone = parser.getAttributeValue(null, "phone");
                String streetAddress = parser.getAttributeValue(null, "office");
                String website = parser.getAttributeValue(null, "link");
                reps.add(new Representative(name, party, twoDigitState, district, phone, streetAddress, website));
            }
            else
            {
                continue;
            }
        }
        return reps;
    }
}