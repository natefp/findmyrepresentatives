package com.wordpress.smartsystemdesign.findmyrepresentatives;

import java.io.Serializable;

/**
 * Created by Mammoth on 5/15/2015.
 */
public class Representative implements Serializable
{
    public final String name;
    public final String nameFormatted;
    public final Party party;
    public final String state;
    public final String twoDigitState;
    public final String districtOrSeat;
    public final String phone;
    public final String streetAddress;
    public final String website;
    public final SenateOrHouse senateOrHouse;

    public Representative(String name, String party, String twoDigitState, String districtOrSeat, String phone, String streetAddress, String website)
    {
        this.name = name;
        // Add party letter after name
        this.nameFormatted = name + ", (" + party + ")";

        if(party.equals("R"))
        {
            this.party = Party.Republican;
        }
        else if(party.equals("D"))
        {
            this.party = Party.Democrat;
        }
        else
        {
            this.party = Party.Independent;
        }

        if (isInteger(districtOrSeat))
        {
            senateOrHouse = SenateOrHouse.House;
        } else
        {
            senateOrHouse = SenateOrHouse.Senate;
        }

        this.state = States.statesMap.get(twoDigitState);
        this.twoDigitState = twoDigitState;

        if (senateOrHouse == SenateOrHouse.House)
        {
            this.districtOrSeat = "District " + districtOrSeat;
        } else
        {
            this.districtOrSeat = districtOrSeat;
        }

        this.phone = phone;
        this.streetAddress = streetAddress;
        this.website = website;
    }

    private static boolean isInteger(String s)
    {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++)
        {
            if(Character.digit(s.charAt(i), 10) < 0) return false;
        }
        return true;
    }
}