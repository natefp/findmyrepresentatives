package com.wordpress.smartsystemdesign.findmyrepresentatives;

/**
 * Created by Mammoth on 5/15/2015.
 */
public enum Party
{
    Republican, Democrat, Independent
}
