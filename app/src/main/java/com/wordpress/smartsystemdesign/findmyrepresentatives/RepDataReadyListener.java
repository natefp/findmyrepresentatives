package com.wordpress.smartsystemdesign.findmyrepresentatives;

import java.util.List;

/**
 * Created by Mammoth on 5/15/2015.
 */
public interface RepDataReadyListener
{
    public void repDataReady(List<Representative> reps);
}
