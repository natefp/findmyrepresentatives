package com.wordpress.smartsystemdesign.findmyrepresentatives;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class NameSearchActivity extends ActionBarActivity implements AdapterView.OnItemSelectedListener
{
    EditText nameEditText;
    Button findButton;
    private boolean isSenator = true;
    private boolean userIsInteracting;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name_search);
        nameEditText = (EditText)findViewById(R.id.name_edit_text);
        findButton = (Button)findViewById(R.id.find_button);
        Spinner houseSenateSpinner = (Spinner)findViewById(R.id.house_senate_spinner);
        houseSenateSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> spinnerArrayAdapter = ArrayAdapter.createFromResource(this,
                R.array.House_Senate_array, android.R.layout.simple_spinner_item);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        houseSenateSpinner.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_name_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickFindButton(View view)
    {
        Intent intent;

        if(view == findButton)
        {
            intent = new Intent(this, ResultsActivity.class);
            intent.putExtra("name", nameEditText.getText().toString());
            intent.putExtra("isSenator", isSenator);
        }
        else
        {
            return;
        }

        startActivity(intent);
    }

    @Override
    public void onUserInteraction()
    {
        super.onUserInteraction();
        userIsInteracting = true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(userIsInteracting)  // check if user is selecting from spinner or if this callback is firing on first layout
        {
            if(position == 0)
            {
                isSenator = true;
            }
            else
            {
                isSenator = false;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
    }
}
